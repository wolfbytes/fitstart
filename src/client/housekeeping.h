#ifndef HOUSEKEEPING_H
#define HOUSEKEEPING_H

#include <vector>
#include <string>

using namespace std;

/*
 * normally I would only comment on what each part does, instead of explaining
 * how it works, but since this will need to be read and understood quickly by
 * the judges, I will explain how it works anyways
 */

// here is a struct that only contains a function pointer: it's to make passing
// around function pointers easier
struct fnptr {
	int (*fn)(vector <string>);
};

struct cmd_struct {
	string cmd;
	fnptr func;
};

// todo: deal with non-exsistent commands (i.e. user error) later
fnptr resolve_subcmds(vector <string> arg_list, cmd_struct subcmds[])
{
	fnptr ret_f;
	for (int i = 0; i < arg_list.size(); ++i) {
		if (arg_list[1] == subcmds[i].cmd) {
			ret_f.fn = subcmds[i].func.fn;
			return ret_f;
		}
	}
}

int run_subcmds(vector <string> arg_list, cmd_struct subcmds[])
{
	fnptr func;
	func = resolve_subcmds(arg_list, subcmds);
	arg_list.erase(arg_list.begin());
	return func.fn(arg_list);
}

#endif
