let SessionLoad = 1
let s:so_save = &g:so | let s:siso_save = &g:siso | setg so=0 siso=0 | setl so=-1 siso=-1
let v:this_session=expand("<sfile>:p")
silent only
cd ~/wolfbytes/fitstart/src/client
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +45 ~/wolfbytes/fitstart/NOTE_TO_JUDGES.md
badd +8 ~/wolfbytes/fitstart/doc/technical-doc/resolving-subcommands.md
badd +18 ~/wolfbytes/fitstart/README.md
badd +24 ~/wolfbytes/fitstart/src/client/fitstart.cpp
badd +5 ~/wolfbytes/fitstart/src/client/Makefile
badd +36 ~/wolfbytes/fitstart/src/client/housekeeping.h
badd +1 ~/wolfbytes/fitstart/src/client/make:\ \*\*\*\ \[Makefile
argglobal
%argdel
edit ~/wolfbytes/fitstart/src/client/fitstart.cpp
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 6 - ((5 * winheight(0) + 31) / 63)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
6
normal! 021|
tabnext 1
if exists('s:wipebuf') && getbufvar(s:wipebuf, '&buftype') isnot# 'terminal'
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 winminheight=1 winminwidth=1 shortmess=filnxtToOFA
let s:sx = expand("<sfile>:p:r")."x.vim"
if filereadable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &g:so = s:so_save | let &g:siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
