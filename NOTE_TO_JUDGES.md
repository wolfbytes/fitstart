Since we don't know how much time you have to look through each team's
individual project, we've assembled 3 different documents to explain the inner
workings of FitStart.

If you only have the time between us and when the next team comes in, read the
top-level README (it's the text with a big "fitstart" at the top on the home
page).

If you have ~5 minutes, [`brief-overview`] should suffice.

If you have longer than that
If you have enough time to review the code, it has quite a bit of comments to
aide understanding.

There is also [`technical-doc`] for very in-depth explanations of specific
parts of `fitstart`.

user-man is meant to be able to be read by everybody (at least we hope so!)
project-doc is documentation about how we made the project and the steps we
went through every day.
technical-doc assumes a decent amount of knowledge of C++ syntax, although
if you know basic programming concepts you _should_ be fine.

The judges can essentially use this document as an explanation to how fitstart
works and how to deal with this repo. There's more documentation in `doc`, but
depending on how much time there is this file could work better.

This git repo mostly lives on GitHub, since that's what more people are
familiar with. All of the links that direct you to files and directories in the
Markdown files are GitHub links, though you should be able to replace
`www.github.com/teamwolfbytes/...` with `www.gitlab.com/wolfbytes/...`, though
at that point you might as well just manually go through the project.

There will be a LOT of comments, since I'm assuming you guys don't have much
time to review the code. Normally I would only comment on what each part does,
instead of explaining how it works, but since this will need to be read and
understood quickly by the judges, I will explain how it works anyway. Each
comment is a pretty brief overview of how the below function/block of code/stuff
works. There's more in-depth technical documention in `doc/technical-doc`.
# add link here

There's some slightly hard to explain stuff 


If you do decide to read through `doc` (link), please read the
respective `README.md` of each directory. They will explain
