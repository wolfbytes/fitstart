# Explanation of project structure

## Directory structure

fitstart
|
|-LICENSE
|
|-README.md
|
|-src
| |
| |-client
| | |
| | +-(various files)
| |
| +-server
|   |
|   +-(various files)
|
+-doc
  |
  |-user-man		<- where the manpage is stored (not created yet)
  |
  |-technical-doc	<- technical documentation of fitstart
  |
  +-project-doc		<- documentation of the process of our project
